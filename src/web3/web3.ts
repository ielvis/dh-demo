import axios from 'axios';

const {
  buildContractClass, buildTypeClasses, bsv,
} = require('scryptlib');

const WEB3_VERSION = '0.0.2';

// eslint-disable-next-line import/prefer-default-export
export class web3 {
  static version() {
    return WEB3_VERSION;
  }

  static async loadContract(url) {
    return axios
      .get(url, {
        timeout: 10000,
      })
      .then((res) => {
        const contractClass = buildContractClass(res.data);
        return {
          contractClass,
          types: buildTypeClasses(contractClass),
        };
      });
  }

  static async deploy(contract, amountInContract) {
    const tx = new bsv.Transaction();
    tx.addOutput(
      new bsv.Transaction.Output({
        script: contract.lockingScript,
        satoshis: amountInContract,
      }),
    );
    const rawTx = tx.toString('hex');
    // eslint-disable-next-line no-undef
    const relayOneSendTest = await relayone.send(rawTx);
    return relayOneSendTest.rawTx;
  }

  static async call(contractUtxo, cbBuildTx) {
    const tx = new bsv.Transaction();

    tx.addInput(
      new bsv.Transaction.Input({
        prevTxId: contractUtxo.txId,
        outputIndex: contractUtxo.outputIndex,
        script: new bsv.Script(), // placeholder
        output: new bsv.Transaction.Output({
          script: contractUtxo.script,
          satoshis: contractUtxo.satoshis,
        }),
      }),
    );
    cbBuildTx(tx);
    const rawTx = tx.toString('hex');
    return rawTx;
  }
}
